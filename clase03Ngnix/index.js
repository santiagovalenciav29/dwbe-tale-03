const express = require('express')
const app = express()
const port = 3000

app.get('/', (req,res) =>{
    res.send('testiando express desde el index, vamos!!');
})

app.listen(port, ()=>{
    console.log(`andando desde el puerto ${port}`)
})
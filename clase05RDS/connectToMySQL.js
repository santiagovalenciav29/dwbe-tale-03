const mysql = require('mysql2/promise');

exports.getAllStudents = async function getAllStudents() {
    const con = await mysql.createConnection ({
        host: "my-first-mysql-rds-instance.c9df6demoy16.us-east-1.rds.amazonaws.com",
        user: "admin",
        password: "3ST6l5S4w6C",
        database: "RDS_FirstExample"
    });

    const query = "SELECT * FROM Students";
    const [results,] = await con.execute(query, null);

    return results;
}